using UnityEngine.SceneManagement;
using UnityEngine;

public class C_Dialog : MonoBehaviour {
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;
    [SerializeField] private Frontground frontground;
    [SerializeField] private Dialog dialog;
    [SerializeField] private Clicker clicker;

    private S_Dialog curDialog;
    private int curPhrase = 0;
    private int countPhrases;
    private bool isEndDialog;

    private void Awake() {
        Application.targetFrameRate = 60;

        frontground.OnAfterShow.AddListener(ChangeScene);

        clicker.OnClick.AddListener(NextPhrase);

        curDialog = settings.GetDialog(save.Get(settings.FractionType));
        countPhrases = curDialog.CountPhrases;
    }

    private void Start() {
        frontground.Hide();
        NextPhrase();
    }

    private void NextPhrase() {
        if (curPhrase == countPhrases) {
            if (!isEndDialog) {
                isEndDialog = true;
                EndDialog();
            }
            return;
        }
        dialog.SetPhrase(curDialog.GetPhrase(curPhrase));
        ++curPhrase;
    }

    private void EndDialog() {
        frontground.Show();
    }

    public void Skip() => ChangeScene();

    private void ChangeScene() {
        if (settings.IsBattle) {
            SceneManager.LoadScene((int)E_Scene.Battle);
        } else {
            SceneManager.LoadScene((int)E_Scene.StoryRoom);
        }
    }
}
