using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class C_Upgrade : MonoBehaviour {
    [Header("Clickers")]
    [SerializeField] private Clicker clickerBack;

    [Header("Modules")]
    [SerializeField] private Frontground frontground;
    [SerializeField] private UpgradeMenu menu;

    [Header("Game")]
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;

    [Header("ThemesImages")]
    [SerializeField] private Image background;
    [SerializeField] private Image backImg;

    [Space]
    [SerializeField] private List<UpgradeSkill> skills;

    private UpgradeSkill curSkillView;
    private AD_Skill curSkill;
    private int curLevel;
    private D_Fraction fraction;

    private void Awake() {
        Application.targetFrameRate = 60;

        clickerBack.OnClick.AddListener(Back);

        for (int i = 0; i < skills.Count; ++i) {
            skills[i].OnSelect.AddListener(Select);
            skills[i].UpdateView(ref save);
            skills[i].ID = i;
        }

        frontground.OnAfterShow.AddListener(() => SceneManager.LoadScene((int)E_Scene.FractionMenu));
        menu.OnUpgrade.AddListener(Upgrade);
    }

    private void Start() {
        fraction = settings.Fraction;

        S_SpritesUpgrade sprites = fraction.Sprites.Upgrade;
        background.sprite = sprites.Background;
        backImg.sprite = sprites.Back;

        frontground.Hide();
    }

    public void Select(int id) {
        curSkillView = skills[id];
        curSkill = curSkillView.Skill;
        curLevel = save.Get(curSkill.GetSkillType());
        curSkillView.UpdateView(ref save);
        menu.UpdateMenu(curSkill, curLevel);
        menu.gameObject.SetActive(true);
    }

    private void Upgrade() {
        if (!IsValidUpgrade()) return;
        Buy(curSkill.GetUpgradeCosts(curLevel));
        save.Add(curSkill.GetSkillType(), 1);
        menu.UpdateMenu(curSkill, curLevel);
        curSkillView.UpdateView(ref save);
    }

    private bool IsValidUpgrade() {
        return IsValidByRang(curSkill.GetUpgradeRangs(curLevel)) &&
            IsValidByCost(curSkill.GetUpgradeCosts(curLevel)) &&
            curSkill.IsHaveNextLevel(curLevel);
    }

    private bool IsValidByRang(List<S_FractionRang> fractions) {
        for (int i = 0; i < fractions.Count; ++i) {
            if (fractions[i].Rang > save.Get(fraction.Type)) return false;
        }
        return true;
    }

    private bool IsValidByCost(List<S_Source> costs) {
        for(int i = 0; i < costs.Count; ++i) {
            if (costs[i].Source > save.Get(costs[i].Type)) return false;
        }
        return true;
    }

    private void Buy(List<S_Source> sources) {
        for(int i = 0; i < sources.Count; ++i) {
            save.Add(sources[i].Type, -sources[i].Source);
        }
    }

    public void Back() {
        frontground.Show();
    }
}
