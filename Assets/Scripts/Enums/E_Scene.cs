public enum E_Scene : byte {
    StartMenu,
    Fractions,
    FractionMenu,
    GrindRoom,
    StoryRoom,
    Dialog,
    Battle,
    Upgrade
}
