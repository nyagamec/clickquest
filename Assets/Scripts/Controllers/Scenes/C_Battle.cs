using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class C_Battle : MonoBehaviour {
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;
    [SerializeField] private Image backgroundImg;
    [SerializeField] private C_Monster monster;
    [SerializeField] private C_Player player;
    [SerializeField] private Frontground frontground;

    private bool isStory;

    private bool IsAttack {
        set {
            monster.IsAttack = value;
            player.IsAttack = value;
        }
    }

    void Awake() {
        Application.targetFrameRate = 60;

        isStory = settings.IsStory;
        AD_Quest quest = settings.IsStory ?
            settings.GetStoryQuest(save.Get(settings.FractionType)) :
            settings.GrindQuest;

        backgroundImg.sprite = quest.Background;
        monster.SetMonster(quest.Monster);
        
        monster.OnAttack.AddListener(player.GetDamage);
        monster.OnDead.AddListener(() => BattleResult(true));
        
        player.OnAttack.AddListener(monster.GetDamage);
        player.OnDead.AddListener(() => BattleResult(false));

        frontground.OnAfterHide.AddListener(() => IsAttack = true);
        frontground.OnAfterShow.AddListener(() => {
            if(isStory) SceneManager.LoadScene((int)E_Scene.Dialog);
            else SceneManager.LoadScene((int)E_Scene.GrindRoom);
        });
    }

    private void Start() {
        settings.IsBattle = true;
        frontground.Hide();
    }

    private void BattleResult(bool res) {
        settings.IsBattle = false;
        settings.IsSuccessBattle = res;
        IsAttack = false;
        if (res) {
            if(isStory) StoryWin();
            else GrindWin();
        }
        frontground.Show();
    }

    private void StoryWin() {
        D_Fraction fraction = settings.Fraction;
        S_UltimateSkill ult = fraction.UltSkill;
        save.Add(fraction.Type, 1);
        if (ult.ActivateRang == save.Get(fraction.Type)) {
            save.Set(ult.Type, ult.Mult);
        }
    }

    private void GrindWin() {
        S_Source source = settings.GrindQuest.Source;
        save.Add(source.Type, source.Source * save.Get(E_UltimateSkill.Sources));
    }
}
