using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SourceView : MonoBehaviour {
    [SerializeField] private E_Source type;
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;
    [SerializeField] private GUISprites GUISprites;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI text;

    [Header("ThemesImages")]
    [SerializeField] private Image sourceFrame;

    private void Start() {
        image.sprite = GUISprites.Get(type);
        text.text = save.Get(type).ToString();

        S_SpritesGrindRoom sprites = settings.Fraction.Sprites.GrindRoom;
        sourceFrame.sprite = sprites.SourceFrame;
    }
}
