using UnityEngine;
using UnityEngine.SceneManagement;

public class C_SplashScreen : MonoBehaviour {
    [SerializeField] private Frontground frontground;

    private void Awake() {
        Application.targetFrameRate = 60;
        frontground.OnAfterSplash.AddListener(LoadFractions);
    }

    private void Start() {
        frontground.Splash();
    }

    public void LoadFractions() {
        SceneManager.LoadScene((int)E_Scene.Fractions);
    }
}
