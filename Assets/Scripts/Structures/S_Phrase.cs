using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct S_Phrase {
    [SerializeField] private Sprite backGround;
    [Multiline]
    [SerializeField] private string text;
    [SerializeField] private List<S_DialogPerson> persons;

    public string Text => text;
    public S_DialogPerson GetPerson(int index) => persons[index];
    public int PersonsCount => persons.Count;
    public Sprite BackGround => backGround;
}
