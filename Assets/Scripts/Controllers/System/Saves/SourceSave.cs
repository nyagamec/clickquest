using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SourceSave : A_Save {
    private Dictionary<E_Source, int> save = new Dictionary<E_Source, int>();
    private List<S_Source> defaultSources = new List<S_Source>();

    [HideInInspector] public UnityEvent OnInitDefaults = new UnityEvent();

    public int Get(E_Source type) {
        if (save.ContainsKey(type)) {
            return save[type];
        } else {
            OnInitDefaults.Invoke();
            int def = GetDefaultValue(type);
            save[type] = def;
            return def;
        }
    }

    public void SetDefaults(ref List<S_Source> defaults) {
        for (int i = 0; i < defaults.Count; ++i) {
            defaultSources.Add(defaults[i]);
        }
    }

    public void Add(E_Source type, int value) {
        if (save.ContainsKey(type))
            save[type] += value;
        else
            save[type] = value;
    }

    private int GetDefaultValue(E_Source type) {
        int res = 0;
        for (int i = 0; i < defaultSources.Count; ++i) {
            if (defaultSources[i].Type == type)
                res = defaultSources[i].Source;
        }
        return res;
    }

    protected override string GetPath() => $"{Application.persistentDataPath}/SourcesSave.dat";

    protected override string ZippingToJSON() {
        List<SaveKeyValue> res = new List<SaveKeyValue>();
        foreach(KeyValuePair<E_Source, int> pair in save) {
            SaveKeyValue val = new SaveKeyValue {
                key = (int)pair.Key,
                value = pair.Value
            };
            res.Add(val);
        }
        return JsonUtility.ToJson(
            new Data {
                data = res
            }
        );
    }

    protected override void UnzippingFromJSON(string json) {
        List<SaveKeyValue> load = JsonUtility.FromJson<Data>(json).data;
        for(int i = 0; i < load.Count; ++i) {
            save[(E_Source)load[i].key] = load[i].value;
        }
    }

    [System.Serializable]
    private struct Data {
        public List<SaveKeyValue> data;
    }

    [System.Serializable]
    private struct SaveKeyValue {
        public int key;
        public int value;
    }
}
