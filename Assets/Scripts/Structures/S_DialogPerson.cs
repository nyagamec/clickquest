using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct S_DialogPerson {
    [SerializeField] private Sprite sprite;
    [SerializeField] private E_Position position;

    public Sprite Sprite => sprite;
    public E_Position Position => position;
}
