using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class Frontground : MonoBehaviour {
    [SerializeField] private Vector3 time;
    [SerializeField] private Color color;
    [SerializeField] private Image image;

    [HideInInspector] public UnityEvent OnAfterSplash = new UnityEvent();
    [HideInInspector] public UnityEvent OnAfterShow = new UnityEvent();
    [HideInInspector] public UnityEvent OnAfterHide = new UnityEvent();

    private float _time;
    private float percent;

    private void Awake() {
        image.enabled = true;
    }

    public void Splash() {
        StartCoroutine(ESplash());
    }

    public void Show() {
        StartCoroutine(EShow());
    }

    public void Hide() {
        StartCoroutine(EHide());
    }

    private IEnumerator ESplash() {
        yield return StartCoroutine(EHide());
        yield return new WaitForSeconds(time.y);
        yield return StartCoroutine(EShow());
        OnAfterSplash.Invoke();
    }

    private IEnumerator EShow() {
        _time = 0;
        while (_time < time.z) {
            percent = _time / time.z;
            _time += Time.fixedDeltaTime;
            color.a = percent;
            image.color = color;
            yield return new WaitForFixedUpdate();
        }
        OnAfterShow.Invoke();
    }

    private IEnumerator EHide() {
        _time = 1;
        while (_time > 0) {
            percent = _time / time.x;
            _time -= Time.fixedDeltaTime;
            color.a = percent;
            image.color = color;
            yield return new WaitForFixedUpdate();
        }
        OnAfterHide.Invoke();
    }
}
