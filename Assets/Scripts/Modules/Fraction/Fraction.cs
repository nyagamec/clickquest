using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class Fraction : MonoBehaviour, IPointerClickHandler {
    [SerializeField] private D_Fraction fraction;
    [SerializeField] private Image skillImg;

    [HideInInspector] public UnityEvent<D_Fraction> OnClick = new UnityEvent<D_Fraction>();

    public E_Fraction Type => fraction.Type;

    public void TryShowSkill(int rang) {
        bool isActiveSkill = rang >= fraction.UltSkill.ActivateRang;
        skillImg.gameObject.SetActive(isActiveSkill);

        if (isActiveSkill) {
            skillImg.sprite = fraction.UltSkill.Sprite;
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        OnClick.Invoke(fraction);
    }
}
