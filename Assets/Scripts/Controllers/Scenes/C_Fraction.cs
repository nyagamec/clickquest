using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class C_Fraction : MonoBehaviour {
    [SerializeField] private Frontground frontground;
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;
    [SerializeField] private List<Fraction> fractions;

    private void Awake() {
        Application.targetFrameRate = 60;
        frontground.OnAfterShow.AddListener(() => SceneManager.LoadScene((int)E_Scene.FractionMenu));
    }

    private void Start() {
        for(int i = 0; i < fractions.Count; ++i) {
            fractions[i].OnClick.AddListener(SelectFraction);
            fractions[i].TryShowSkill(save.Get(fractions[i].Type));
        }
        frontground.Hide();
    }

    public void SelectFraction(D_Fraction fraction) {
        settings.Fraction = fraction;
        frontground.Show();
    }
}
