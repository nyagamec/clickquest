using UnityEngine;

[CreateAssetMenu(menuName = "Datas/Monster", fileName = "MonsterData")]
public class D_Monster : ScriptableObject {
    [SerializeField] private Sprite sprite;
    [SerializeField] private float health;
    [SerializeField] private float interval;
    [SerializeField] private float power;

    public Sprite Sprite => sprite;
    public float Health => health;
    public float Interval => interval;
    public float Power => power;
}
