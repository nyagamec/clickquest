using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RangSave : A_Save {
    private Dictionary<E_Fraction, int> save = new Dictionary<E_Fraction, int>();
    private List<S_FractionRang> defaultRangs = new List<S_FractionRang>();

    [HideInInspector] public UnityEvent OnInitDefaults = new UnityEvent();

    public int Get(E_Fraction type) {
        if (save.ContainsKey(type)) {
            return save[type];
        } else {
            OnInitDefaults.Invoke();
            int def = GetDefaultValue(type);
            save[type] = def;
            return def;
        }
    }

    public void SetDefaults(ref List<S_FractionRang> defaults) {
        for (int i = 0; i < defaults.Count; ++i) {
            defaultRangs.Add(defaults[i]);
        }
    }

    public void Add(E_Fraction type, int value) {
        if(save.ContainsKey(type))
            save[type] += value;
        else
            save[type] = value;
    }

    private int GetDefaultValue(E_Fraction type) {
        int res = 0;
        for (int i = 0; i < defaultRangs.Count; ++i) {
            if (defaultRangs[i].Type == type)
                res = defaultRangs[i].Rang;
        }
        return res;
    }

    protected override string GetPath() => $"{Application.persistentDataPath}/RangSave.dat";

    protected override string ZippingToJSON() {
        List<SaveKeyValue> res = new List<SaveKeyValue>();
        foreach (KeyValuePair<E_Fraction, int> pair in save) {
            SaveKeyValue val = new SaveKeyValue {
                key = (int)pair.Key,
                value = pair.Value
            };
            res.Add(val);
        }
        return JsonUtility.ToJson(
            new Data {
                data = res
            }
        );
    }

    protected override void UnzippingFromJSON(string json) {
        List<SaveKeyValue> load = JsonUtility.FromJson<Data>(json).data;
        for (int i = 0; i < load.Count; ++i) {
            save[(E_Fraction)load[i].key] = load[i].value;
        }
    }

    [System.Serializable]
    private struct Data {
        public List<SaveKeyValue> data;
    }

    [System.Serializable]
    private struct SaveKeyValue {
        public int key;
        public int value;
    }
}
