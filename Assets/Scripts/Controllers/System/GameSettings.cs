using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {
    [SerializeField] private List<D_Fraction> _fractions;

    private void Start() {
        Debug.Log($"Save direction is here: {Application.persistentDataPath}");
    }

    public E_Fraction FractionType => (E_Fraction) PlayerPrefs.GetInt("FractionType");

    public D_Fraction Fraction {
        get {
            string fractionName = PlayerPrefs.GetString("Fraction");
            for(int i = 0; i < _fractions.Count; ++i) {
                if (_fractions[i].name.Equals(fractionName)) {
                    return _fractions[i];
                }
            }
            return null;
        }
        set {
            PlayerPrefs.SetString("Fraction", value.name);
            PlayerPrefs.SetInt("FractionType", (int) value.Type);
        }
    }

    public bool IsStory {
        get => PlayerPrefs.GetInt("IsStory") == 1;
        set => PlayerPrefs.SetInt("IsStory", value ? 1 : 0);
    }

    //public AD_Quest Quest {
    //    get {
    //        if (IsStory) {
    //            return StoryQuest;
    //        } else {
    //            return GrindQuest;
    //        }
    //    }
    //}

    public D_StoryQuest GetStoryQuest(int rang) => Fraction.GetStoryQuest(rang);

    public D_GrindQuest GrindQuest {
        get => Fraction.GetGrindQuest(PlayerPrefs.GetString("GrindQuest"));
        set => PlayerPrefs.SetString("GrindQuest", value.name);
    }

    public S_Dialog GetDialog(int rang) {
        D_StoryQuest quest;
        if (IsBattle) {
            quest = GetStoryQuest(rang);
            return quest.StartDialog;
        } else {
            if (IsSuccessBattle) {
                quest = GetStoryQuest(rang - 1);
                return quest.WinDialog;
            } else {
                quest = GetStoryQuest(rang);
                return quest.LoseDialog;
            }
        }
    }

    public bool IsBattle {
        get => PlayerPrefs.GetInt("IsBattle") == 1;
        set => PlayerPrefs.SetInt("IsBattle", value ? 1 : 0);
    }
    public bool IsSuccessBattle {
        get => PlayerPrefs.GetInt("IsSuccessBattle") == 1;
        set => PlayerPrefs.SetInt("IsSuccessBattle", value ? 1 : 0);
    }
}
