using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public abstract class A_Save {
    protected abstract string GetPath();
    protected abstract string ZippingToJSON();
    protected abstract void UnzippingFromJSON(string json);

    public void Load() {
        string path = GetPath();
        if (!File.Exists(path)) return;
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = new FileStream(path, FileMode.Open);
        string json = (string)formatter.Deserialize(file);
        file.Close();
        UnzippingFromJSON(json);
    }

    public void Save() {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream file = File.Open(GetPath(), FileMode.OpenOrCreate);
        formatter.Serialize(file, ZippingToJSON());
        file.Close();
    }

    public void ClearSave() {
        File.Delete(GetPath());
    }
}
