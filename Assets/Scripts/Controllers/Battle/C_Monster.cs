using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class C_Monster : MonoBehaviour {
    [SerializeField] private Image monsterImg;
    [SerializeField] private Image healthImg;

    [HideInInspector] public UnityEvent<float> OnAttack = new UnityEvent<float>();
    [HideInInspector] public UnityEvent OnDead = new UnityEvent();

    private float health;
    private float maxHealth;
    private float interval;
    private float power;

    private bool isAttack;

    public bool IsAttack {
        set {
            isAttack = value;
            if (value) StartCoroutine(Attack());
        }
    }

    public void SetMonster(D_Monster monster) {
        monsterImg.sprite = monster.Sprite;
        maxHealth = health = monster.Health;
        interval = monster.Interval;
        power = monster.Power;
        monsterImg.fillAmount = 1;
    }

    private IEnumerator Attack() {
        while (isAttack) {
            OnAttack.Invoke(power);
            yield return new WaitForSeconds(interval);
        }
    }

    public void GetDamage(float damage) {
        health = Mathf.Clamp(health - damage, 0, maxHealth);
        healthImg.fillAmount = health / maxHealth;
        if(health == 0) {
            OnDead.Invoke();
        }
    }
}
