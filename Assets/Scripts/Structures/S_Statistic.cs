using UnityEngine;

[System.Serializable]
public struct S_Statistic {
    public Sprite icon;
    public float value;
    public float nextValue;
}
