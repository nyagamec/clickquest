using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Datas/Fraction", fileName = "FractionData")]
public class D_Fraction : ScriptableObject {
    [SerializeField] private E_Fraction type;
    [SerializeField] private S_UltimateSkill ultSkill;
    [SerializeField] private S_FractionSprites sprites;
    [SerializeField] private List<D_StoryQuest> storyQuests;
    [SerializeField] private List<D_GrindQuest> grindQuests;

    public E_Fraction Type => type;
    public S_UltimateSkill UltSkill => ultSkill;
    public S_FractionSprites Sprites => sprites;
    public int MaxRang => storyQuests.Count;
    public D_GrindQuest GetGrindQuest(string name) => GetQuest(ref grindQuests, name);

    public D_StoryQuest GetStoryQuest(int rang) => storyQuests[rang];
    public D_GrindQuest GetRandomGrindQuest(int rang) {
        List<int> questIndexes = new List<int>();
        for (int i = 0; i < grindQuests.Count; ++i) {
            if (grindQuests[i].Rang.x <= rang && rang <= grindQuests[i].Rang.y) {
                questIndexes.Add(i);
            }
        }
        return questIndexes.Count > 0 ? grindQuests[questIndexes[Random.Range(0, questIndexes.Count)]] : null;
    }

    private T GetQuest<T>(ref List<T> quests, string name) where T : AD_Quest {
        for (int i = 0; i < quests.Count; ++i) {
            if (quests[i].name.Equals(name)) return quests[i];
        }
        return null;
    }
}
