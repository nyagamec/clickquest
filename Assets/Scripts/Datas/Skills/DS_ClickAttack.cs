using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Datas/Skills/ClickAttack", fileName = "ClickAttack")]
public class DS_ClickAttack : AD_Skill {
    [SerializeField] private Sprite powerSprite;
    [SerializeField] private Sprite energySprite;
    [SerializeField] private List<S_Level> data;

    public float GetPower(int level) => data[level].settings.power;
    public float GetEnergy(int level) => data[level].settings.energy;

    public override E_Skill GetSkillType() => E_Skill.Attack;
    public override bool IsHaveNextLevel(int level) => level < data.Count - 1;
    public override Sprite GetIcon(int level) => data[level].description.icon;
    public override string GetName(int level) => data[level].name;
    public override string GetDescription(int level) => data[level].description.description;
    public override List<S_Source> GetUpgradeCosts(int level) => level < data.Count - 1 ? data[level + 1].cost : null;
    public override List<S_FractionRang> GetUpgradeRangs(int level) => level < data.Count - 1 ? data[level + 1].fraction : null;
    
    public override List<S_Statistic> GetStatistics(int level) {
        List<S_Statistic> resList = new List<S_Statistic>();

        resList.Add(new S_Statistic {
            icon = powerSprite,
            value = data[level].settings.power,
            nextValue = level < data.Count - 1 ? data[level + 1].settings.power : 0
        });

        resList.Add(new S_Statistic {
            icon = energySprite,
            value = data[level].settings.energy,
            nextValue = level < data.Count - 1 ? data[level + 1].settings.energy : 0
        });

        return resList;
    }

    [System.Serializable]
    private struct S_Level {
        public string name;
        public S_Description description;
        public S_Settings settings;
        public List<S_Source> cost;
        public List<S_FractionRang> fraction;
    }

    [System.Serializable]
    private struct S_Settings {
        public float power;
        public float energy;
    }

    [System.Serializable]
    private struct S_Description {
        public Sprite icon;
        [Multiline(7)]
        public string description;
    }
}
