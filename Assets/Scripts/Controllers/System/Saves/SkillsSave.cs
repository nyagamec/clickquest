using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillsSave : A_Save {
    private Dictionary<E_Skill, int> save = new Dictionary<E_Skill, int>();
    private List<S_SkillLevel> defaultLevels = new List<S_SkillLevel>();

    [HideInInspector] public UnityEvent OnInitDefaults = new UnityEvent();

    public int Get(E_Skill type) {
        if (save.ContainsKey(type)) {
            return save[type];
        } else {
            OnInitDefaults.Invoke();
            int def = GetDefaultValue(type);
            save[type] = def;
            return def;
        }
    }

    public void SetDefaults(ref List<S_SkillLevel> defaults) {
        for(int i = 0; i < defaults.Count; ++i) {
            defaultLevels.Add(defaults[i]);
        }
    }

    public void Add(E_Skill type, int value) {
        if (save.ContainsKey(type))
            save[type] += value;
        else
            save[type] = value;
    }

    private int GetDefaultValue(E_Skill type) {
        int res = 0;
        for (int i = 0; i < defaultLevels.Count; ++i) {
            if (defaultLevels[i].Type == type)
                res = defaultLevels[i].Level;
        }
        return res;
    }

    protected override string GetPath() => $"{Application.persistentDataPath}/SkillsSave.dat";

    protected override string ZippingToJSON() {
        List<SaveKeyValue> res = new List<SaveKeyValue>();
        foreach (KeyValuePair<E_Skill, int> pair in save) {
            SaveKeyValue val = new SaveKeyValue {
                key = (int)pair.Key,
                value = pair.Value
            };
            res.Add(val);
        }
        return JsonUtility.ToJson(
            new Data {
                data = res
            }
        );
    }

    protected override void UnzippingFromJSON(string json) {
        List<SaveKeyValue> load = JsonUtility.FromJson<Data>(json).data;
        for (int i = 0; i < load.Count; ++i) {
            save[(E_Skill)load[i].key] = load[i].value;
        }
    }

    [System.Serializable]
    private struct Data {
        public List<SaveKeyValue> data;
    }

    [System.Serializable]
    private struct SaveKeyValue {
        public int key;
        public int value;
    }
}
