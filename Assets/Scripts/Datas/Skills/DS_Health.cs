using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Datas/Skills/Health", fileName = "Health")]
public class DS_Health : AD_Skill {
    [SerializeField] private Sprite maxSprite;
    [SerializeField] private List<S_Level> levels;

    public float GetMax(int level) => levels[level].settings.max;

    public override E_Skill GetSkillType() => E_Skill.Health;
    public override bool IsHaveNextLevel(int level) => level < levels.Count - 1;
    public override string GetName(int level) => levels[level].name;
    public override Sprite GetIcon(int level) => levels[level].description.icon;
    public override string GetDescription(int level) => levels[level].description.description;
    public override List<S_Source> GetUpgradeCosts(int level) => level < levels.Count - 1 ? levels[level + 1].cost : null;
    public override List<S_FractionRang> GetUpgradeRangs(int level) => level < levels.Count - 1 ? levels[level + 1].fraction : null;

    public override List<S_Statistic> GetStatistics(int level) {
        List<S_Statistic> resList = new List<S_Statistic>();

        resList.Add(new S_Statistic {
            icon = maxSprite,
            value = levels[level].settings.max,
            nextValue = level < levels.Count - 1 ? levels[level + 1].settings.max : 0
        });

        return resList;
    }

    [System.Serializable]
    private struct S_Level {
        public string name;
        public S_Description description;
        public S_Settings settings;
        public List<S_Source> cost;
        public List<S_FractionRang> fraction;
    }

    [System.Serializable]
    private struct S_Settings {
        public float max;
    }

    [System.Serializable]
    private struct S_Description {
        public Sprite icon;
        [Multiline(7)]
        public string description;
    }
}
