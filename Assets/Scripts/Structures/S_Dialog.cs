using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct S_Dialog {
    [SerializeField] private List<S_Phrase> phrases;
    public S_Phrase GetPhrase(int index) => phrases[index];
    public int CountPhrases => phrases.Count;
}
