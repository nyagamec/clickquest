using UnityEngine;

[CreateAssetMenu(menuName = "Datas/Sources", fileName = "SourceData")]
public class D_Source : ScriptableObject {
    [SerializeField] private E_Source type;
    [SerializeField] private Sprite icon;

    public E_Source Type => type;
    public Sprite Icon => icon;
}
