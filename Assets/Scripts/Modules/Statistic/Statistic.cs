using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class Statistic : MonoBehaviour {
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI statTxt;

    public Sprite Icon { set => icon.sprite = value; }
    public string StatTxt { set => statTxt.text = value; }
    public Color Color { set => statTxt.color = value; }

    public void Delete() {
        Destroy(gameObject);
    }
}
