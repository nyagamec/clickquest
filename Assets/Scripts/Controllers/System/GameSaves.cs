using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameSaves : MonoBehaviour {
    [SerializeField] private List<S_FractionRang> defaultRangs;
    [SerializeField] private List<S_Source> defaultSources;
    [SerializeField] private List<S_SkillLevel> defaultSkillsLevels;
    [SerializeField] private List<S_UltimateSkill> defaultUltimateSkillsActives;

    private RangSave rangSave = new RangSave();
    private SourceSave sourceSave = new SourceSave();
    private SkillsSave skillSave = new SkillsSave();
    private UltimateSkillsSave ultimateSkillsSave = new UltimateSkillsSave();

    [HideInInspector] public UnityEvent OnLoaded = new UnityEvent();

#if UNITY_EDITOR
    private void Awake() {
        InitSavesEvents();
        Load();
        OnLoaded.Invoke();
    }

    private void OnDestroy() {
        Save();
    }
#elif UNITY_ANDROID
    private void OnApplicationPause(bool pause) {
        if (pause) {
            Save();
        } else {
            InitSavesEvents();
            Load();
            OnLoaded.Invoke();
        }
    }
#endif

    private void InitSavesEvents() {
        rangSave.OnInitDefaults.AddListener(() => rangSave.SetDefaults(ref defaultRangs));
        sourceSave.OnInitDefaults.AddListener(() => sourceSave.SetDefaults(ref defaultSources));
        skillSave.OnInitDefaults.AddListener(() => skillSave.SetDefaults(ref defaultSkillsLevels));
        ultimateSkillsSave.OnInitDefaults.AddListener(() => ultimateSkillsSave.SetDefaults(ref defaultUltimateSkillsActives));
    }

    private void Load() {
        rangSave.Load();
        sourceSave.Load();
        skillSave.Load();
        ultimateSkillsSave.Load();
    }

    private void Save() {
        rangSave.Save();
        sourceSave.Save();
        skillSave.Save();
        ultimateSkillsSave.Save();
    }

    public int Get(E_Fraction type) => rangSave.Get(type);
    public int Get(E_Source type) => sourceSave.Get(type);
    public int Get(E_Skill type) => skillSave.Get(type);
    public int Get(E_UltimateSkill type) => ultimateSkillsSave.Get(type);

    public void Set(E_UltimateSkill type, int value) => ultimateSkillsSave.Set(type, value);

    public void Add(E_Fraction type, int value) => rangSave.Add(type, value);
    public void Add(E_Source type, int value) => sourceSave.Add(type, value);
    public void Add(E_Skill type, int value) => skillSave.Add(type, value);

}
