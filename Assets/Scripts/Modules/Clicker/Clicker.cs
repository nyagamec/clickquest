using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.Events;

public class Clicker : MonoBehaviour, IPointerClickHandler {
    [HideInInspector] public UnityEvent OnClick = new UnityEvent();
    public void OnPointerClick(PointerEventData eventData) {
        OnClick.Invoke();
    }
}
