using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static UnityEngine.EventSystems.EventTrigger;

public class C_Player : MonoBehaviour {
    [SerializeField] private GameSaves save;
    [SerializeField] private DS_Health healthSkill;
    [SerializeField] private DS_Energy energySkill;
    [SerializeField] private DS_ClickAttack attackSkill;
    [SerializeField] private Image healthImg;
    [SerializeField] private Image energyImg;
    [SerializeField] private Clicker clicker;

    [HideInInspector] public UnityEvent<float> OnAttack = new UnityEvent<float>();
    [HideInInspector] public UnityEvent OnDead = new UnityEvent();

    private float health;
    private float maxHealth;
    private float energy;
    private float maxEnergy;
    private float attackEnergy;
    private float power;
    private float energyRenewalPower;
    private float energyRenewalInterval;

    private bool isAttack;
    private bool isRenewalEnergy;
    
    public bool IsAttack { set => isAttack = value; }

    private void Awake() {
        clicker.OnClick.AddListener(Click);

        maxHealth = health = healthSkill.GetMax(save.Get(E_Skill.Health)) * save.Get(E_UltimateSkill.Health);

        int level = save.Get(E_Skill.Energy);
        maxEnergy = energy = energySkill.GetMax(level);
        energyRenewalPower = energySkill.GetPower(level);
        energyRenewalInterval = energySkill.GetInterval(level);

        level = save.Get(E_Skill.Attack);
        attackEnergy = attackSkill.GetEnergy(level);
        power = attackSkill.GetPower(level) * save.Get(E_UltimateSkill.Power);

        healthImg.fillAmount = 1;
        energyImg.fillAmount = 1;
    }

    public void Click() {
        if (!isAttack | energy - attackEnergy < 0) return;
        OnAttack.Invoke(power);
        energy = Mathf.Clamp(energy - attackEnergy, 0, maxEnergy);
        energyImg.fillAmount = energy / maxEnergy;
        if (!isRenewalEnergy) {
            StartCoroutine(RenewalEnergy());
        }
    }

    public void GetDamage(float damage) {
        health = Mathf.Clamp(health - damage, 0, maxHealth);
        healthImg.fillAmount = health / maxHealth;
        if (health == 0) {
            OnDead.Invoke();
        }
    }

    private IEnumerator RenewalEnergy() {
        isRenewalEnergy = true;
        while (energy < maxEnergy) {
            energy = Mathf.Clamp(energy + energyRenewalPower, 0, maxEnergy);
            energyImg.fillAmount = energy / maxEnergy;
            yield return new WaitForSeconds(energyRenewalInterval);
        }
        isRenewalEnergy = false;
    }
}
