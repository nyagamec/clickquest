using System.Collections.Generic;
using UnityEngine;

public abstract class AD_Skill : ScriptableObject {
    private string id = "";

    public string ID {
        get {
            if (id == "") id = System.Guid.NewGuid().ToString();
            return id;
        }
    }

    public abstract E_Skill GetSkillType();
    public abstract bool IsHaveNextLevel(int level);
    public abstract Sprite GetIcon(int level);
    public abstract string GetName(int level);
    public abstract string GetDescription(int level);
    public abstract List<S_Source> GetUpgradeCosts(int level);
    public abstract List<S_FractionRang> GetUpgradeRangs(int level);
    public abstract List<S_Statistic> GetStatistics(int level);
}
