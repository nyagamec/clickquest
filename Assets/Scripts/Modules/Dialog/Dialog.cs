using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Image background;
    [SerializeField] private Image leftPerson;
    [SerializeField] private Image centerPerson;
    [SerializeField] private Image rightPerson;

    public void SetPhrase(S_Phrase phrase) {
        ShowPersons(false);
        text.text = phrase.Text;
        background.sprite = phrase.BackGround;
        for(int i = 0; i < phrase.PersonsCount; ++i) {
            S_DialogPerson dialogPerson = phrase.GetPerson(i);
            switch (dialogPerson.Position) {
                case E_Position.Left:
                    leftPerson.enabled = true;
                    leftPerson.sprite = dialogPerson.Sprite;
                    break;
                case E_Position.Center:
                    centerPerson.enabled = true;
                    centerPerson.sprite = dialogPerson.Sprite;
                    break;
                case E_Position.Right:
                    rightPerson.enabled = true;
                    rightPerson.sprite = dialogPerson.Sprite;
                    break;
            }
        }
    }

    private void ShowPersons(bool isShow) {
        leftPerson.enabled = isShow;
        centerPerson.enabled = isShow;
        rightPerson.enabled = isShow;
    }
}
