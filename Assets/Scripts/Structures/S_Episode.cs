using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct S_Episode {
    [SerializeField] private Sprite background;
    [SerializeField] private D_Monster monster;
    [SerializeField] private S_Dialog startDialog;
    [SerializeField] private S_Dialog endDialog;

    public Sprite Background => background;
    public D_Monster Monster => monster;
    public S_Dialog StartDialog => startDialog;
    public S_Dialog EndDialog => endDialog;
}
