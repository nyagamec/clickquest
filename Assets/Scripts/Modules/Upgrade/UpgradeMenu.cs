using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UpgradeMenu : MonoBehaviour {
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI nameTxt;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private Transform statistics;
    [SerializeField] private Transform fractions;
    [SerializeField] private Transform sources;
    [SerializeField] private Transform upgradeBtn;
    [SerializeField] private Statistic statisticPref;
    [SerializeField] private GameSettings settings;
    [SerializeField] private GameSaves save;
    [SerializeField] private GUISprites sprites;

    [Header("ThemesImages")]
    [SerializeField] private Image skillFrame;
    [SerializeField] private Image nameImg;
    [SerializeField] private Image descriptionImg;
    [SerializeField] private Image statisticsImg;
    [SerializeField] private Image rangsImg;
    [SerializeField] private Image sourcesImg;
    [SerializeField] private Image upgradeImg;
    [SerializeField] private Image closeImg;

    private AD_Skill skill;
    private int level;
    private D_Fraction fraction;

    [HideInInspector] public UnityEvent OnUpgrade = new UnityEvent();

    private void Start() {
        fraction = settings.Fraction;

        S_SpritesUpgrade sprites = fraction.Sprites.Upgrade;
        skillFrame.sprite = sprites.SkillFrame;
        nameImg.sprite = sprites.Name;
        descriptionImg.sprite = sprites.Description;
        statisticsImg.sprite = sprites.Statistics;
        rangsImg.sprite = sprites.Rangs;
        sourcesImg.sprite = sprites.Sources;
        upgradeImg.sprite = sprites.Upgrade;
        closeImg.sprite = sprites.Close;
    }

    public void UpdateMenu(AD_Skill skill, int level) {
        this.skill = skill;
        this.level = level;

        icon.sprite = skill.GetIcon(level);
        nameTxt.text = skill.GetName(level);
        description.text = skill.GetDescription(level);
        UpdateStatistics();

        upgradeBtn.gameObject.SetActive(skill.IsHaveNextLevel(level));
        if (skill.IsHaveNextLevel(level)) {
            fractions.gameObject.SetActive(true);
            sources.gameObject.SetActive(true);
            UpdateFractions();
            UpdateSources();
        } else {
            fractions.gameObject.SetActive(false);
            sources.gameObject.SetActive(false);
        }
    }

    private void UpdateStatistics() {
        ClearTransform(statistics);
        FullStatistics();
    }

    private void UpdateFractions() {
        ClearTransform(fractions);
        FullFractions();
    }
    
    private void UpdateSources() {
        ClearTransform(sources);
        FullSources();
    }

    private void FullStatistics() {
        List<S_Statistic> statisticsList = skill.GetStatistics(level);
        for (int i = 0; i < statisticsList.Count; ++i) {
            Statistic newStat = Instantiate(statisticPref, statistics);
            newStat.Icon = statisticsList[i].icon;
            newStat.StatTxt = skill.IsHaveNextLevel(level) ?
                              $"{statisticsList[i].value} (+{statisticsList[i].nextValue - statisticsList[i].value})" :
                              statisticsList[i].value.ToString();
        }
    }

    private void FullFractions() {
        List<S_FractionRang> fractionsList = skill.GetUpgradeRangs(level);
        for (int i = 0; i < fractionsList.Count; ++i) {
            Statistic newStat = Instantiate(statisticPref, fractions);
            newStat.Icon = sprites.Get(fractionsList[i].Type);
            bool isValid = save.Get(fraction.Type) - fractionsList[i].Rang >= 0;
            newStat.Color = isValid ? Color.green : Color.red;
            newStat.StatTxt = fractionsList[i].Rang.ToString();
        }
    }

    private void FullSources() {
        List<S_Source> sourcesList = skill.GetUpgradeCosts(level);
        for (int i = 0; i < sourcesList.Count; ++i) {
            Statistic newStat = Instantiate(statisticPref, sources);
            newStat.Icon = sprites.Get(sourcesList[i].Type);
            bool isValid = save.Get(sourcesList[i].Type) - sourcesList[i].Source >= 0;
            newStat.Color = isValid ? Color.green : Color.red;
            newStat.StatTxt = sourcesList[i].Source.ToString();
        }
    }

    private void ClearTransform(Transform t) {
        for(int i = 0; i < t.childCount; ++i) {
            if(t.GetChild(i).GetComponent<Statistic>())
                t.GetChild(i).GetComponent<Statistic>().Delete();
        }
    }

    public void Upgrade() {
        OnUpgrade.Invoke();
    }

    public void Close() {
        gameObject.SetActive(false);
    }
}
