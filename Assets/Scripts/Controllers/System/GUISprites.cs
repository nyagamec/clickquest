using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUISprites : MonoBehaviour {
    [SerializeField] private List<S_Sprite<E_Source>> sourcesSprite;
    [SerializeField] private List<S_Sprite<E_Fraction>> fractionsSprite;

    public Sprite Get(E_Source type) {
        for(int i = 0; i < sourcesSprite.Count; ++i) {
            if (sourcesSprite[i].type == type) {
                return sourcesSprite[i].sprite;
            }
        }
        return null;
    }

    public Sprite Get(E_Fraction type) {
        for(int i = 0; i < fractionsSprite.Count; ++i) {
            if (fractionsSprite[i].type == type) {
                return fractionsSprite[i].sprite;
            }
        }
        return null;
    }

    [System.Serializable]
    private struct S_Sprite<T> {
        public T type;
        public Sprite sprite;
    }
}
